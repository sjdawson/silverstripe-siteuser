# SilverStripe SiteUser #

A module set to bridge the gap between SilverStripe and the [Eden PHP library](http://eden.openovate.com/) for the purpose of allowing users to log in using their Facebook, Twitter or Google+ accounts. It also provides the option to create an account without using one of the aforementioned services. 