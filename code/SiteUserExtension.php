<?php

/**
 * SiteUserExtension
 *
 * Extends SiteTree with functionality thats needed for the SiteUser module
 *
 * @author Steven Dawson <steve@djdawson.co.uk>
 * @package silverstripe-site-user
 * @subpackage extensions
 */
class SiteUserExtension extends DataExtension
{
    /**
     * An array of all of the available authenticators for this module
     */
    public static $authenticators = array();



    /**
     * A pre-defined path for the authentication procedures to use
     */
    public static $authPath = "user/authenticate";



    /**
     * Perform actions whenever this class is constructed, such as coralling all
     * the available authenticators into the class property, and including the
     * Eden library (http://eden.openovate.com/) for use throughout
     */
    public function __construct()
    {
        parent::__construct();

        $registered_authenticators = ClassInfo::implementorsOf("SiteUserAuth");
        self::$authenticators = array_combine($registered_authenticators, $registered_authenticators);

        require_once(__DIR__.'/libs/eden.php');
    }



    /**
     * Works out which of the implementers of SiteUserAuth can deal with
     * requests for a given network. It should only ever return classes that
     * implement the SiteUserAuth interface
     *
     * @return SiteUserAuth
     */
    public static function getAuthLibrary($network)
    {
        foreach (self::$authenticators as $auth_lib)
            if ($auth_lib::$network == $network)
                return new $auth_lib;

        user_error("Network '".$network."' not found.", E_USER_ERROR);
    }



    /**
     * Generates a login url for a network for use in templates, with a return path
     *
     * @return string
     */
    public function LoginURL($network)
    {
        return "/user/login/".$network."/?returnto=".urlencode(Director::get_current_page()->Link());
    }



    /**
     * Generates a logout url for use in templates, with a return path
     *
     * @return string
     */
    public function LogoutURL()
    {
        return "/user/logout/?returnto=".urlencode(Director::get_current_page()->Link());
    }



    /**
     * Generates a login URL for use in templates to the standalone page
     *
     * @return string
     */
    public function LoginPageURL()
    {
        return "/user/login/";
    }



    /**
     * Generates a registration URL for use in templates to the standalone page
     *
     * @return string
     */
    public function RegisterPageURL()
    {
        return "/user/register/";
    }



    /**
     * Return all of the data about the currently logged in member
     *
     * @return SiteUser
     */
    public function getSiteUser()
    {
        return SiteUser::logged_in();
    }



    /**
     * Return errors on the authentication form and clear the session message
     *
     * @return string
     */
    public function getAuthFormError()
    {
        $message = Session::get('site_user_auth_form_error');

        if ($message != "")
            $message = "<span class='alert-box warning'>".$message."</span>";

        Session::clear('site_user_auth_form_error');

        return $message;
    }



    /**
     * Return the email address to re-populate the form on an error
     *
     * @return string
     */
    public function getAuthFormData()
    {
        $data = Session::get('site_user_auth_form.email');

        return $data;
    }



    /**
     * Return errors on the registration form and clear the session message
     *
     * @return string
     */
    public function getRegFormError()
    {
        $message = Session::get('site_user_reg_form_error');

        if ($message != "")
            $message = "<span class='alert-box warning'>".$message."</span>";

        Session::clear('site_user_reg_form_error');

        return $message;
    }



    /**
     * Return the email address to re-populate the form on an error. We don't
     * pass the password back through for security
     *
     * @return string
     */
    public function getRegFormData()
    {
        $data = Session::get('site_user_reg_form.email');

        return $data;
    }



    /**
     * Return errors on the forgotten password form and clear the session message
     *
     * @return string
     */
    public function getForgottenFormError()
    {
        $message = Session::get('site_user_reset_form_error');

        if ($message != "")
            $message = "<span class='alert-box warning'>".$message."</span>";

        Session::clear('site_user_reset_form_error');

        return $message;
    }



    /**
     * Return the token value from the GET string
     *
     * @return string
     */
    public function getResetFormToken()
    {
        return Controller::curr()->request->getVar('tkn');
    }



    /**
     * Peforms a check to see whether the request to the current reset token is
     * still valid, and if so will allow the reset form to be shown. This will
     * still return false even if it doesn't find a user, so the outcome is
     * two-fold in that respect of checking for validity
     *
     * @return boolean
     */
    public function getTokenIsStillValid()
    {
        $token = Controller::curr()->request->getVar('tkn');

        $user = SiteUser::get()->filter
        (
            array
            (
                'PasswordForgottenToken' => $token
            )
        )->first();

        $expires = $user->PasswordForgottenTime + 900; // 15 minutes for reset

        return (time() < $expires) ? true : false;
    }



    /**
     * Check to see if the user is registering
     */
    public function IsRegistering()
    {
        $register = Controller::curr()->request->getVar('register');

        return (isset($register) && $register == "do") ? true : false;
    }
}
