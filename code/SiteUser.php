<?php

/**
 * SiteUser
 *
 * Core DataObject for a SiteUser
 *
 * @author Steven Dawson <steve@djdawson.co.uk>
 * @package silverstripe-site-user
 * @subpackage models
 */
class SiteUser extends DataObject
{
    /**
     * An array of data types to be stored on this object
     */
    private static $db = array
    (
        "Name" => "Varchar(255)",
        "DisplayName" => "Varchar(140)",
        "Email" => "Varchar(255)",
        "Password" => "Text",
        "PasswordForgottenToken" => "Text",
        "PasswordForgottenTime" => "Varchar(15)",
        "DateOfBirth" => "Date",
        "Gender" => "Enum('Male,Female,Not Specified', 'Not Specified')",
    );



    /**
     * Find or make a user, based on the data that's sent through in the array.
     * The $data array should carry all of the fields it wishes to populate this
     * object with on creation.
     *
     * @return SiteUser
     */
    public function findOrMakeUser($data=array())
    {
        if (trim($data['Email']) == "")
            user_error('Can\'t find or make a user when no email address is specified', E_USER_WARNING);

        $caller = get_called_class();

        $user = $caller::get()->filter('Email', $data['Email'])->first();

        if (!$user) // Create user
        {
            if (!isset($data['Password']))
                $data['Password'] = Member::create_new_password();

            $user = $caller::create($data);
            $user->Password = md5($data['Password']);

            $user->write();
        }

        return $user;
    }



    /**
     * A user can be authenticated using their email address and a password that
     * has been specified by themselves during registration.
     *
     * @param array $data
     * @return SiteUser|bool
     */
    public function authenticateWithPassword($data=array())
    {
        $caller = get_called_class();

        $user = $caller::get()->filter
        (
            array
            (
                'Email' => $data['Email'],
                'Password' => $data['Password']
            )
        )->first();

        if ($user)
            return $user;
        else
        {
            Session::set('site_user_auth_form_error', 'Oops. That\'s an incorrect email address, password or both. Please try again.');
            return false;
        }
    }



    /**
     * We can all be forgetful, so let's give the user a way to reset their
     * password without having to ask a site admin to help them. Time is also
     * stored so that we can invalidate tokens for added security
     *
     * @param array $data
     */
    public function forgottenPassword($data=array())
    {
        $caller = get_called_class();

        $user = $caller::get()->filter(
            array(
                'Email' => $data['Email']
            ))->first();

        if ($user)
        {
            $token = sha1(md5(time()));
            $user->PasswordForgottenToken = $token;
            $user->PasswordForgottenTime = time();
            $user->write();

            $to = $user->Email;
            $domain_parts = explode(".", $_SERVER['HTTP_HOST']);
            array_shift($domain_parts);
            $domain = implode(".", $domain_parts);
            $from = SiteConfig::current_site_config()->Title." <noreply@".$domain.">";
            $subject = SiteConfig::current_site_config()->Title." - Password Reset";
            $body = "";

            $email = new Email($from, $to, $subject, $body);
            $email->setTemplate('ForgottenPassword');

            $email->populateTemplate(array(
                'URL' => Director::absoluteBaseURL()."user/forgotten/?tkn=".$token,
                'User' => $user,
                'SiteConfig' => SiteConfig::current_site_config()
            ));

            $email->send();
        }
    }



    /**
     * And this is what happens when the above function is successful in
     * assisting a users on changing their old/forgotten password
     *
     * @param array $data
     */
    public function resetPassword($data=array())
    {
        $caller = get_called_class();

        $user = $caller::get()->filter
        (
            array
            (
                'PasswordForgottenToken' => $data['Token']
            )
        )->first();

        if ($user)
        {
            $user->PasswordForgottenToken = "";
            $user->PasswordForgottenTime = "";
            $user->Password = md5($data['Password']);
            $user->write();

            $to = $user->Email;
            $domain_parts = explode(".", $_SERVER['HTTP_HOST']);
            array_shift($domain_parts);
            $domain = implode(".", $domain_parts);
            $from = SiteConfig::current_site_config()->Title." <noreply@".$domain.">";
            $subject = SiteConfig::current_site_config()->Title." - Password Reset Successful";
            $body = "";

            $email = new Email($from, $to, $subject, $body);
            $email->setTemplate('ResetPassword');

            $email->populateTemplate(array(
                'URL' => Director::absoluteBaseURL()."user/forgotten/?tkn=".$token,
                'User' => $user,
                'SiteConfig' => SiteConfig::current_site_config()
            ));

            $email->send();
        }
    }



    /**
     * Check the session to see if a user is currently logged in, and if so,
     * return their data to whatever has called this function
     *
     * @return SiteUser|bool
     */
    public static function logged_in()
    {
        $site_user = Session::get("site_user_id");

        if (empty($site_user))
            return false;

        $site_user = (int) $site_user;

        if ($site_user < 1)
            return false;

        $caller = get_called_class();
        $site_user = $caller::get()->byID($site_user);

        if (empty($site_user))
            return false;

        return $site_user;
    }
}
