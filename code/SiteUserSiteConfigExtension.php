<?php

/**
 * SiteUserSiteConfigExtension
 *
 * Adds extra fields to the SiteConfig element that allows us to specify API
 * keys and other data that we'll need for the various networks this module
 * sets out to implement
 *
 * @author Steven Dawson <steve@djdawson.co.uk>
 * @package silverstripe-site-user
 * @subpackage extensions
 */
class SiteUserSiteConfigExtension extends DataExtension
{
    private static $db = array
    (
        "FacebookAppID" => "Text",
        "FacebookAppSecret" => "Text",
        "GooglePlusAppID" => "Text",
        "GooglePlusAppSecret" => "Text",
        "GooglePlusApiKey" => "Text"
    );


    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldToTab('Root.SiteUser', HeaderField::create('Facebook', 'Facebook'));
        $fields->addFieldToTab('Root.SiteUser', TextField::create('FacebookAppID'));
        $fields->addFieldToTab('Root.SiteUser', TextField::create('FacebookAppSecret'));

        $fields->addFieldToTab('Root.SiteUser', HeaderField::create('GooglePlus', 'Google Plus'));
        $fields->addFieldToTab('Root.SiteUser', TextField::create('GooglePlusAppID'));
        $fields->addFieldToTab('Root.SiteUser', TextField::create('GooglePlusAppSecret'));
        $fields->addFieldToTab('Root.SiteUser', TextField::create('GooglePlusApiKey'));
    }
}
