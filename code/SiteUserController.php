<?php

/**
 * SiteUserController
 *
 * In here, we'll take care of the "business logic" that needs to be on a
 * controller to operate, but doesn't need any pages in the CMS to work.
 * The majority of the methods within this controller should end up in a
 * redirect to somewhere else on the site. Most commonly this will be the page
 * that the user was last on before interaction with the SiteUser module
 *
 * @author Steven Dawson <steve@djdawson.co.uk>
 * @package silverstripe-site-user
 * @subpackage controllers
 */
class SiteUserController extends Page_Controller
{
    /**
     * An array of methods that can be called publicly by URL
     */
    private static $allowed_actions = array
    (
        "index",
        "register",
        "login",
        "logout",
        "authenticate",
        "forgotten"
    );



    /**
     * This is where we'll route when there's no other action specified. We'll
     * check if there's a logged in user, and redirect them accordingly
     */
    public function index(SS_HTTPRequest $request)
    {
        if (SiteUser::logged_in())
            $this->redirect('/');
        else
            $this->redirect('login');
    }



    /**
     * We route here when a user has forgotten their password
     *
     * @return SiteUserController
     */
    public function forgotten()
    {
        return $this->renderWith(array('ResetPasswordForm', 'Page'));
    }



    /**
     * Start the login procedure for any given network, storing a return URL so
     * that we can point the user back to where they were before logging in
     */
    public function login()
    {
        // Set the page to return to
        Session::set('returnto', $this->request->getVar('returnto'));

        if ($this->request->param('ID') != "")
        {
            $lib = SiteUserExtension::getAuthLibrary($this->request->param('ID'));
            $this->redirect($lib->getLoginURL());
            return;
        }

        return $this->renderWith(array('LoginPage', 'Page'));
    }



    /**
     * We need to maintain a standalone method for a user to register to the site
     * without the need for social networks, becuase some people prefer that
     */
    public function register()
    {
        return $this->renderWith(array('RegisterPage', 'Page'));
    }


    /**
     * Destroy the session that's active for the currently logged in user
     */
    public function logout()
    {
        Session::clear('site_user_id');
        $this->redirect($this->request->getVar('returnto'));
    }



    /**
     * Perform the authentication procedure for any given network
     */
    public function authenticate()
    {
        $get = $this->request->getVars();
        $network = $this->request->param('ID');

        $lib = SiteUserExtension::getAuthLibrary($this->request->param('ID'));

        $lib->doAuthentication();
    }
}
