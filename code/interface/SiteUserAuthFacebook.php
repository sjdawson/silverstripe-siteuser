<?php

/**
 * SiteUserAuthFacebook
 *
 * This implementation of SiteUserAuth allows us to deal with the Facebook network
 *
 * @author Steven Dawson <steve@djdawson.co.uk>
 * @package silverstripe-site-user
 * @subpackage interface
 */
class SiteUserAuthFacebook implements SiteUserAuth
{
    /**
     * The machine readable name that this class deals with.
     */
    public static $network = "facebook";



    /**
     * Perform the authentication methods needed for this network. Passes the
     * data we grab from the network back to the SiteUser::findOrMakeUser()
     * method so that we can keep 'em local - in case they decide to stop logging
     * in socially
     */
    public function doAuthentication()
    {
        $auth = $this->getAuthObject();
        $args = Controller::curr()->request;

        // Check that cancel wasn't pressed Facebook side
        if ($args->getVar('error') != "")
            user_error($args->getVar('error')." becuase ".$args->getVar('error_reason'), E_USER_WARNING);
        else
        {
            $access = $auth->getAccess($args['code']);

            if (isset($access['error'])) // We got problems!
                user_error($access['error']['message'], E_USER_WARNING);

            // Get the user's data from Facebook
            Session::set('fb_token', $access['access_token']);
            $graph = eden('facebook')->graph(Session::get('fb_token'))->getUser();

            // Make the data uniform for db insert
            $data['Name'] = (isset($graph['name']))?$graph['name']:"";
            $data['Email'] = (isset($graph['email']))?$graph['email']:"";
            $data['DateOfBirth'] = (isset($graph['birthday']))?date('Y-m-d', strtotime($graph['birthday'])):"";
            $data['Gender'] = (isset($graph['gender']))?ucwords($graph['gender']):"Not Specified";

            // Find us the existing, or make a new user from that data
            $user = SiteUser::create()->findOrMakeUser($data);

            // Set user ID to show we're logged in
            Session::set('site_user_id', $user->ID);
        }

        // Return to set page if set or home if not
        $redirectto = (Session::get('returnto') != "") ? Session::get('returnto') : "/";
        Controller::curr()->redirect($redirectto);
    }



    /**
     * Get Eden's authentication object for this network
     *
     * @return Eden_Facebook_Auth
     */
    public function getAuthObject()
    {
        $config = SiteConfig::current_site_config();
        return eden('facebook')->auth($config->FacebookAppID, $config->FacebookAppSecret, Director::absoluteBaseURL().SiteUserExtension::$authPath."/".self::$network);
    }



    /**
     * Get Eden's authentication object login URL for this network
     *
     * @return Eden_Facebook_Auth
     */
    public function getLoginURL()
    {
        return $this->getAuthObject()->getLoginUrl(array('email', 'user_birthday'));
    }
}
