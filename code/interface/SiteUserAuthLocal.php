<?php

/**
 * SiteUserAuthLocal
 *
 * This implementation of SiteUserAuth allows us to deal with local logins
 *
 * @author Steven Dawson <steve@djdawson.co.uk>
 * @package silverstripe-site-user
 * @subpackage interface
 */
class SiteUserAuthLocal implements SiteUserAuth
{
    /**
     * The machine readable name that this class deals with.
     */
    public static $network = "local";



    /**
     * Perform the authentication methods needed for this network. Since this
     * network is performing locally, there's more need for data validation.
     * We also handle the forgotten password and reset passwords here which
     * streamlines the users perspective of things
     */
    public function doAuthentication()
    {
        $args = Controller::curr()->request;
        $redirectto = (Session::get('returnto') != "")? Session::get('returnto')  : "/" ;

        if ($args->postVar('action_AuthenticateUser'))
        {
            Session::set('site_user_auth_form.email', $args->postVar('Email'));

            if ($args->postVar('Email') == "")
            {
                Session::set('site_user_auth_form_error', 'Oops. Looks like you haven\'t entered your email address');
                return Controller::curr()->redirect($redirectto);
            }

            if (filter_var($args->postVar('Email'), FILTER_VALIDATE_EMAIL) == false)
            {
                Session::set('site_user_auth_form_error', 'Oops. That\'s not a valid email address');
                return Controller::curr()->redirect($redirectto);
            }

            if (trim($args->postVar('Password')) == "" || $args->postVar('Password') == "d41d8cd98f00b204e9800998ecf8427e") // md5 of a blank string
            {
                Session::set('site_user_auth_form_error', 'Oops. Looks like you haven\'t entered your password');
                return Controller::curr()->redirect($redirectto);
            }

            $data['Email'] = $args->postVar('Email');
            $data['Password'] = md5($args->postVar('Password'));

            $user = SiteUser::create()->authenticateWithPassword($data);

            // Set user ID to show we're logged in
            if ($user)
            {
                Session::set('site_user_id', $user->ID);
                Session::clear('site_user_auth_form');
            }
        }
        else if ($args->postVar('action_RegisterUser')) // Let's register a user
        {
            Session::set('site_user_reg_form.email', $args->postVar('Email'));

            if ($args->postVar('Email') == "")
            {
                Session::set('site_user_reg_form_error', 'Oops. Looks like you haven\'t entered an email address');
                return Controller::curr()->redirect($redirectto);
            }

            if (filter_var($args->postVar('Email'), FILTER_VALIDATE_EMAIL) == false)
            {
                Session::set('site_user_reg_form_error', 'Oops. That\'s not a valid email address');
                return Controller::curr()->redirect($redirectto);
            }

            if (trim($args->postVar('Password')) == "" || $args->postVar('Password') == "d41d8cd98f00b204e9800998ecf8427e") // md5 of a blank string
            {
                Session::set('site_user_reg_form_error', 'Oops. Looks like you haven\'t entered a password');
                return Controller::curr()->redirect($redirectto);
            }

            $data['Email'] = $args->postVar('Email');
            $name = explode("@", $args->postVar('Email'));
            $data['Name'] = $name[0];
            $data['Password'] = $args->postVar('Password');

            // Find us the existing, or make a new user from that data
            $user = SiteUser::create()->findOrMakeUser($data);

            // Set user ID to show we're logged in
            Session::set('site_user_id', $user->ID);
            Session::clear('site_user_reg_form');
        }
        else if ($args->postVar('action_ForgottenPassword')) // Let's deal with a forgotten password
        {
            Session::set('site_user_auth_form.email', $args->postVar('Email'));

            if ($args->postVar('Email') == "")
            {
                Session::set('site_user_auth_form_error', 'Oops. Looks like you haven\'t entered your email address');
                return Controller::curr()->redirect($redirectto);
            }

            if (filter_var($args->postVar('Email'), FILTER_VALIDATE_EMAIL) == false)
            {
                Session::set('site_user_auth_form_error', 'Oops. That\'s not a valid email address');
                return Controller::curr()->redirect($redirectto);
            }

            if (trim($args->postVar('Password')) != "")
            {
                Session::set('site_user_auth_form_error', 'Oops. Leave the password blank if you\'d like to reset your password');
                return Controller::curr()->redirect($redirectto);
            }

            $data['Email'] = $args->postVar('Email');

            $user = SiteUser::create()->forgottenPassword($data);

            Session::set('site_user_auth_form_error', 'A password reset has been sent');
        }
        else if ($args->postVar('action_ResetPassword')) // Let's deal with resetting that password
        {
            if (trim($args->postVar('Password')) == "")
            {
                Session::set('site_user_reset_form_error', 'Oops. You haven\'t entered a new password');
                return Controller::curr()->redirectBack();
            }

            if (trim($args->postVar('Password_Confirm')) == "")
            {
                Session::set('site_user_reset_form_error', 'Oops. You haven\'t repeated your password for confirmation');
                return Controller::curr()->redirectBack();
            }

            if (trim($args->postVar('Password_Confirm')) != trim($args->postVar('Password')))
            {
                Session::set('site_user_reset_form_error', 'Oops. Those passwords don\'t match');
                return Controller::curr()->redirectBack();
            }

            $data['Password'] = $args->postVar('Password');
            $data['Token'] = $args->postVar('token');
            $user = SiteUser::create()->resetPassword($data);
        }

        // Return to set page if set or home if not
        Controller::curr()->redirect($redirectto);
    }



    /**
     * Gets the authentication object for this network
     *
     * @return boolean
     */
    public function getAuthObject()
    {
        return false;
    }



    /**
     * Shows the login url for this network
     *
     * @return boolean
     */
    public function getLoginURL()
    {
        return false;
    }
}
