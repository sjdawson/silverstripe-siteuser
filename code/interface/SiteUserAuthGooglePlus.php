<?php

/**
 * SiteUserAuthGooglePlus
 *
 * This implementation of SiteUserAuth allows us to deal with the Google+ network
 *
 * @author Steven Dawson <steve@djdawson.co.uk>
 * @package silverstripe-site-user
 * @subpackage interface
 */
class SiteUserAuthGooglePlus implements SiteUserAuth
{
    /**
     * The machine readable name that this class deals with.
     */
    public static $network = "googleplus";



    /**
     * Perform the authentication methods needed for this network. Passes the
     * data we grab from the network back to the SiteUser::findOrMakeUser()
     * method so that we can keep 'em local - in case they decide to stop logging
     * in socially
     */
    public function doAuthentication()
    {
        $auth = $this->getAuthObject();
        $args = Controller::curr()->request;

        // Check that cancel wasn't pressed Facebook side
        if ($args->getVar('error') != "")
            user_error($args->getVar('error')." becuase ".$args->getVar('error_reason'), E_USER_WARNING);
        else
        {
            $access = $auth->getAccess($args['code']);

            if (isset($access['error'])) // We got problems!
                user_error($access['error'], E_USER_WARNING);

            // Get the user's data from Facebook
            Session::set('gp_token', $access['access_token']);
            $plus = eden('google')->plus(Session::get('gp_token'))->people()->getUserInfo();

            // Make the data uniform for db insert
            $name = "";
            $name .= (isset($plus['name']['givenName']))?$plus['name']['givenName']:"";
            $name .= (isset($plus['name']['familyName']))?" ".$plus['name']['familyName']:"";
            $data['Name'] = $name;

            if (isset($plus['emails']))
                $email = $plus['emails'][0]['value'];
            $data['Email'] = $email;

            $data['Gender'] = (isset($plus['gender']))?ucwords($plus['gender']):"Not Specified";

            // Find us the existing, or make a new user from that data
            $user = SiteUser::create()->findOrMakeUser($data);

            // Set user ID to show we're logged in
            Session::set('site_user_id', $user->ID);
        }

        // Return to set page if set or home if not
        $redirectto = (Session::get('returnto') != "")? Session::get('returnto')  : "/" ;
        Controller::curr()->redirect($redirectto);
    }



    /**
     * Get Eden's authentication object for this network
     *
     * @return Eden_Google_Oauth
     */
    public function getAuthObject()
    {
        $config = SiteConfig::get()->first();
        return eden('google')->auth($config->GooglePlusAppID, $config->GooglePlusAppSecret, Director::absoluteBaseURL().SiteUserExtension::$authPath."/".self::$network);
    }



    /**
     * Get Eden's authentication object login URL for this network
     *
     * @return Eden_Google_Oauth
     */
    public function getLoginURL()
    {
        return $this->getAuthObject()->getLoginUrl(array('email', 'profile'));
    }
}
