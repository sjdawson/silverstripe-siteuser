<?php

/**
 * SiteUserAuth
 *
 * An interface that can be implemented on other authentication libraries so
 * we can authenticate from various sources and the front level code always
 * remains the same, we just tweak/add implementors of this class and the system
 * takes care of the rest
 *
 * @author Steven Dawson <steve@djdawson.co.uk>
 * @package silverstripe-site-user
 * @subpackage interface
 */
interface SiteUserAuth
{
    /**
     * Perform the authentication methods needed for this network which will find
     * or make the user with the data we get back from the OAuth request
     */
    public function doAuthentication();



    /**
     * Get Eden's authentication object for this network
     */
    public function getAuthObject();



    /**
     * Get Eden's authentication object login URL for this network
     */
    public function getLoginURL();
}
