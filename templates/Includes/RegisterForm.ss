<h2>Join $SiteConfig.Title</h2>

<p>Connect with</p>

<a href="/user/login/facebook/?returnto=%2F"><i class="icon-facebook"></i>Facebook</a>
<a href="/user/login/googleplus/?returnto=%2F"><i class="icon-google"></i>Google</a>

<div class="divider">
    <span>OR</span>
</div>

<form action="/user/authenticate/local/?returnto=%2Fuser%2Fregister" method="post" enctype="application/x-www-form-urlencoded">

    <% if $RegFormError %>
        $RegFormError
    <% end_if %>

    <div id="Name" class="field text">
        <label class="left" for="Form_userauthenticatelocal_Name">Name</label>
        <input type="text" name="Name" class="text" id="Form_userauthenticatelocal_Name" placeholder="Name">
    </div>

    <div id="Email" class="field text">
        <label class="left" for="Form_userauthenticatelocal_Email">Email Address</label>
        <input type="text" name="Email" class="text" id="Form_userauthenticatelocal_Email" placeholder="Email Address" <% if $RegFormData %>value="$RegFormData"<% end_if %>>
    </div>

    <div id="Password" class="field text password">
        <label class="left" for="Form_userauthenticatelocal_Password">Password</label>
        <input type="password" name="Password" class="text password" id="Form_userauthenticatelocal_Password" placeholder="Password">
    </div>

    <div class="Disclaimer">
        <p>By clicking create my account, you are agreeing to this site's <a href="#">terms of service</a></p>
    </div>

    <div class="Actions">
        <button type="submit" name="action_RegisterUser" value="Register">Create my account</button>
    </div>

</form>

<div class="divider"></div>

<div>
    <a href="/user/login">Login</a>
</div>